package com.example.bae.combinedtester;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import java.util.List;


public class MainActivity extends Activity implements OnClickListener, SensorEventListener {

// WIFI SCAN 변수들----------------------------------------------------------------------------------
    private static final String TAG = "WIFIScanner";

    //Context
    private Context mContext;

    // WifiManager 변수
    private WifiManager wifimanager;

    // UI 변수들
    private TextView textStatus;
    private Button btnScanStart;
    private Button btnScanStop;

    private int scanCount = 0;

    //wifi스캔 도중에 start나 stop을 연속적으로 누르는 것을 방지하기 위해 만듬
    private int scanStatus = 0;

    String text = "";
    String result = "";

    //wifi 스캔 결과들을 담을 list
    private List<ScanResult> mScanResult; // ScanResult List

    //wifi scan 결과를 Broadcast로 받는다.
    private BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();
            if (action.equals(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION)) {
                getWIFIScanResult(); // get WIFISCanResult
                wifimanager.startScan(); // for refresh
            } else if (action.equals(WifiManager.NETWORK_STATE_CHANGED_ACTION)) {
                mContext.sendBroadcast(new Intent("wifi.ON_NETWORK_STATE_CHANGED"));
            }
        }
    };

// -------------------------------------------------------------------------------------------------
// Barometer 변수들----------------------------------------------------------------------------------

    SensorManager sensorManager;
    Sensor sensor;
    private TextView tvSensorStatus;

// -------------------------------------------------------------------------------------------------

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        //----------------------------WIFI---------------------------------------------

        // Setup UI
        textStatus = (TextView) findViewById(R.id.textStatus);
        btnScanStart = (Button) findViewById(R.id.btnScanStart);
        btnScanStop = (Button) findViewById(R.id.btnScanStop);

        // Setup OnClickListener
        btnScanStart.setOnClickListener(this);
        btnScanStop.setOnClickListener(this);

        // Setup WIFI
        mContext = getApplicationContext();
        wifimanager = (WifiManager) mContext.getSystemService(WIFI_SERVICE);
        Log.d(TAG, "Setup WIfiManager getSystemService");

        // if WIFIEnabled
        if (wifimanager.isWifiEnabled() == false)
            wifimanager.setWifiEnabled(true);

        //------------------------------------------------------------------------------
        //------------------------Barometer---------------------------------------------
        tvSensorStatus = (TextView) findViewById(R.id.a_pressure);

        //센서매니저 인스턴스
        sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        //기압센서 쿼리
        sensor = sensorManager.getDefaultSensor(Sensor.TYPE_PRESSURE);
        //리스너 등록
        sensorManager.registerListener(this, sensor, SensorManager.SENSOR_DELAY_NORMAL);

    }


// WIFI SCAN 함수들----------------------------------------------------------------------------------

    public void getWIFIScanResult() {

        mScanResult = wifimanager.getScanResults(); // ScanResult
        // Scan count
        textStatus.setText("Scan count is \t" + ++scanCount + " times \n");

        textStatus.append("=======================================\n");
        for (int i = 0; i < mScanResult.size(); i++) {
            ScanResult result = mScanResult.get(i);
            textStatus.append((i + 1) + ". SSID : " + result.SSID.toString()
                    + "\t\t RSSI : " + result.level + " dBm\n");
        }
        textStatus.append("=======================================\n");
    }

    public void printToast(String messageToast) {
        Toast.makeText(this, messageToast, Toast.LENGTH_LONG).show();
    }

    public void initWIFIScan() {
        // init WIFISCAN
        scanCount = 0;
        text = "";
        final IntentFilter filter = new IntentFilter(
                WifiManager.SCAN_RESULTS_AVAILABLE_ACTION);
        filter.addAction(WifiManager.NETWORK_STATE_CHANGED_ACTION);
        mContext.registerReceiver(mReceiver, filter);
        wifimanager.startScan();
        Log.d(TAG, "initWIFIScan()");
    }

// -------------------------------------------------------------------------------------------------

//----------------------------LISTENER--------------------------------------------------------------
    @Override
    public void onClick(View view) {

        if (view.getId() == R.id.btnScanStart) {
            if(scanStatus == 0) {
                Log.d(TAG, "OnClick() btnScanStart()");
                printToast("WIFI SCAN !!!");
                initWIFIScan(); // start WIFIScan
                scanStatus = 1;
            }
        }
        if (view.getId() == R.id.btnScanStop) {
            if(scanStatus == 1) {
                Log.d(TAG, "OnClick() btnScanStop()");
                printToast("WIFI STOP !!!");
                mContext.unregisterReceiver(mReceiver); // stop WIFISCan
                scanStatus = 0;
            }
        }
    }


//----------------------------Barometer-------------------------------------------------------------

    @Override
    protected void onResume() {
        super.onResume();
        sensorManager.registerListener(this, sensor, SensorManager.SENSOR_DELAY_NORMAL);
    }

    @Override
    protected void onPause() {
        super.onPause();
        sensorManager.unregisterListener(this);
    }

    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        long timestamp = sensorEvent.timestamp;
        float presure = sensorEvent.values[0];
        presure = (float) (Math.round(presure*100)/100.0); //소수점 2자리 반올림
        //기압을 바탕으로 고도를 계산
        float height = SensorManager.getAltitude(SensorManager.PRESSURE_STANDARD_ATMOSPHERE, presure);
        height = (float) (Math.round(height*100)/100.0);
        //화면에 출력
        tvSensorStatus.setText("기압: "+String.valueOf(presure) +" hPa \n 고도: "+height+"m" );
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }
}
